class ItemsController < ApplicationController
  def index
    json= ' {
        "name": "tshirt",
        "description": "Una camisa de playa",
        "price": "23.34",
        "track_inventory": true,
        "inventory": 23
    }'

    json2= ' {
        "name": "Pantalon",
        "description": "Larga descripción",
        "price": "23.34",
        "track_inventory": true,
        "inventory": 23
    }'
    item_hash = JSON.parse json
    item= Item.new(item_hash['name'], item_hash['description'], item_hash['price'], item_hash['track_inventory'], item_hash['inventory'])

    item_hash = JSON.parse json2
    item2= Item.new(item_hash['name'], item_hash['description'], item_hash['price'], item_hash['track_inventory'], item_hash['inventory'])



    @items = Array.new #=> []
    @items.push(item)
    @items.push(item2)

  end

  def new
    @item= Item.new()
  end

  def edit
    json= ' {
        "name": "tshirt",
        "description": "Una camisa de playa",
        "price": "23.34",
        "track_inventory": true,
        "inventory": 23
    }'
    item_hash = JSON.parse json
    @item= Item.new(item_hash['name'], item_hash['description'], item_hash['price'], item_hash['track_inventory'], item_hash['inventory'])
  end

  def create
    flash[:alert] = 'No se puede crear. [Modo Solo Lectura]'
    #flash[:notice] = 'Item creado exitosamente'
    #flash[:error] = 'Item creado exitosamente'
    redirect_to :action => 'edit', :id => 3
  end

  def update
    flash[:alert] = 'No se puede actualizar. [Modo Solo Lectura]'
    #flash[:notice] = 'Item actualizado exitosamente'
    #flash[:error] = 'Item actualizado exitosamente'
    redirect_to :action => 'edit', :id => 3
  end

  def destroy
    flash[:alert] = 'No se puede eliminar. [Modo Solo Lectura]'
    #flash[:notice] = 'Item actualizado exitosamente'
    #flash[:error] = 'Item actualizado exitosamente'
    redirect_to :action => 'index'
  end

end
