class Item

  def initialize(name='', description='', price='', track_inventory=false, inventory='')
    @name = name
    @description = description
    @price = price
    @track_inventory = track_inventory
    @inventory = inventory
  end

  def name
    @name
  end

  def description
    @description
  end

  def price
    @price
  end

  def track_inventory
    @track_inventory
  end

  def inventory
    @inventory
  end


end