require 'test_helper'

class SettingsControllerTest < ActionController::TestCase
  test "should get account" do
    get :account
    assert_response :success
  end

  test "should get referrals" do
    get :referrals
    assert_response :success
  end

end
