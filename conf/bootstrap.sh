#!/usr/bin/env bash

apt-get update

echo 'mysql-server-5.5 mysql-server/root_password password toor' | debconf-set-selections
echo 'mysql-server-5.5 mysql-server/root_password_again password toor' | debconf-set-selections
apt-get -y install mysql-client mysql-server-5.5 

apt-get -y install php5  php-apc php5-mysql php5-dev php5-intl curl

apt-get install -y git

#install rails
#https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-on-ubuntu-14-04-using-rvm

\curl -sSL https://get.rvm.io | bash -s stable --rails
source ~/.rvm/scripts/rvm

#install node
#https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server
add-apt-repository ppa:chris-lea/node.js
apt-get update
apt-get -y install nodejs



